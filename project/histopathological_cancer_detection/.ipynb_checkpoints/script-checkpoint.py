import os
from pathlib import Path
from utils import *
from fastai.vision import *
import pandas as pd
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
# from io import FileIO

BATCH_SIZE = 64 # default batch size
SIZE = 224

# set project directories and files
ROOT_DIRECTORY = "C:/Users/diana/Documents/licenta/"
DATA_DIRECTORY = ROOT_DIRECTORY + "data"
TRAIN_PATH = DATA_DIRECTORY + "/train"
TEST_PATH = DATA_DIRECTORY + "/test"
TRAIN_LABELS = DATA_DIRECTORY + "/train_labels.csv"

data_path = Path(DATA_DIRECTORY)
print_files(data_path)

# data analysis
labels = pd.read_csv(TRAIN_LABELS)
labels.head()

labels.info()

labels['label'].value_counts().plot(kind='bar', title='Negative/positive ratio')

labels['label'].value_counts()

# get random samples from the data
random_data = shuffle(labels)

figure, ax = plt.subplots(2,4, figsize=(20,10))
figure.suptitle('Scans of lymph node sections',fontsize=20)

for image_iterator, image_id in enumerate(random_data[random_data['label'] == 0]['id'][:4]):
    path = os.path.join(TRAIN_PATH, image_id)
    ax[0,image_iterator].imshow(read_image(path + '.tif'))
    # show the centered 32x32 patch
    patch = patches.Rectangle((32,32),32,32,linewidth=5,edgecolor='r',facecolor='none', linestyle=':', capstyle='projecting')
    ax[0,image_iterator].add_patch(patch)
ax[0,0].set_ylabel('Negative samples', size='large')

for image_iterator, image_id in enumerate(random_data[random_data['label'] == 1]['id'][:4]):
    path = os.path.join(TRAIN_PATH, image_id)
    ax[1,image_iterator].imshow(read_image(path + '.tif'))
    patch = patches.Rectangle((32,32),32,32,linewidth=5,edgecolor='r',facecolor='none', linestyle=':', capstyle='projecting')
    ax[1,image_iterator].add_patch(patch)
ax[1,0].set_ylabel('Positive samples', size='large')

transforms = get_transforms(do_flip=True, flip_vert=True)


# apply transforms to the data
data = ImageDataBunch.from_csv(DATA_DIRECTORY,
                               ds_tfms=transforms,
                               size=SIZE,
                               suffix=".tif",
                               folder="train",
                               test="test",
                               csv_labels="train_labels.csv",
                               bs=BATCH_SIZE)

# image normalization
data.normalize(imagenet_stats)

# peek at data
data.show_batch(rows=5, figsize=(15, 15))

# CNN Resnet50
learn = cnn_learner(data, models.resnet50, metrics=error_rate, callback_fns=ShowGraph)