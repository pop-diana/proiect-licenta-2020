import cv2

def print_files(path):
    for current_file in path.iterdir():
        print(current_file)
        
def read_image(path):
    bgr_image = cv2.imread(path)
    # flip image to rgb
    b,g,r = cv2.split(bgr_image)
    rgb_image = cv2.merge([r,g,b])
    return rgb_image